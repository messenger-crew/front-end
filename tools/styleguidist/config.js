const path = require('path')
const productionWebpackConfig = require('../webpack/production.config')
const devWebpackConfig = require('../webpack/dev.config')

module.exports = {
    title: 'Documentation',
    components: path.resolve(__dirname, '../../src/components/**/**/**.js'),
    serverHost: '0.0.0.0',
    serverPort: 3000,
    styleguideDir: path.resolve(__dirname, '../../styleguide'),
    getComponentPathLine(componentPath) {
        const name = path.basename(componentPath)
        const dir = path.dirname(componentPath).replace('../../', '')
        return `${dir}/${name}`
    },
    require: [
        'bootstrap-loader',
        path.resolve(__dirname, 'style-guide-wrapper.js')
    ],
    webpackConfig(env) {
        const webpackConfig = 'production' === env ? productionWebpackConfig : devWebpackConfig
        if (!webpackConfig.resolve) {
            webpackConfig.resolve = {}
        }
        if (!webpackConfig.resolve.alias) {
            webpackConfig.resolve.alias = {}
        }
        webpackConfig.resolve.alias = {
            'rsg-components/Wrapper': path.join(__dirname, 'style-guide-wrapper')
        }
        return webpackConfig
    }
}
