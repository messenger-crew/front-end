import React, { Component } from 'react'
import { Provider } from 'react-redux'
import config from '../../src/config'
import createReduxResources from '../../src/core/create-redux-resources'
import { MemoryRouter } from 'react-router-dom'

export default class StyleGuideWrapper extends Component {
    constructor(props) {
        super(props)
        const resources = createReduxResources(config)
        this.store = resources.store
        this.history = resources.reduxHistory
    }

    render() {
        return (
            <Provider store={this.store}>
                <MemoryRouter history={this.history}>
                    {this.props.children}
                </MemoryRouter>
            </Provider>
        )
    }
}
