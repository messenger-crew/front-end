#!/usr/bin/env sh
set -e
DIR=$(dirname $0)
cd $DIR/../../

echo "-- Run 'clean' task --"

rm -rf dist

echo 'Done.'
