#!/usr/bin/env sh
set -e
DIR=$(dirname $0)
cd $DIR/../../

echo "==== Run 'ui-test' task ==="

./node_modules/.bin/casperjs test tools/ui-test/pixel-perfect-test-runner.js
