#!/usr/bin/env sh
set -e
DIR=$(dirname $0)
cd $DIR/../../

echo "==== Run 'Production build' task ==="

./tools/bin/clean.sh
./tools/bin/init-app.sh

./tools/bin/lint.sh
./tools/bin/test.sh

echo "build project..."

#run production build
. ./.settings

FE__HOST=$FE__HOST \
FE__PORT=$FE__PORT \
FE__PATH_APP=$FE__PATH_APP \
FE__PATH_BUILD=$FE__PATH_BUILD \
FE__PATH_NODE_MODULES=$FE__PATH_NODE_MODULES \
NODE_ENV=production \
    ./node_modules/.bin/webpack \
        -p \
        --config tools/webpack/production.config.js \
        --progress \
        --colors \
        --display-error-details

echo "Done."