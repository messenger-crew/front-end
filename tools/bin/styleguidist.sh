#!/usr/bin/env sh
set -e
DIR=$(dirname $0)
cd $DIR/../../

echo "-- Run 'styleguidist' task --"

key="$1";
shift $(( $# > 0 ? 1 : 0  ))

case $key in
    server)
        NODE_ENV=test \
            ./node_modules/.bin/styleguidist \
                --config ./tools/styleguidist/config.js \
                server
    ;;
    build)
        NODE_ENV=production \
            ./node_modules/.bin/styleguidist  \
                --config ./tools/styleguidist/config.js \
                build
    ;;
esac
