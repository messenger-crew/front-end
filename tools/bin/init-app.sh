#!/usr/bin/env sh

DIR=$(dirname $0)
cd $DIR/../../

echo "-- Run 'init-app' task --"

if [ ! -d "./node_modules" ]; then
    npm install
fi
