#!/usr/bin/env sh
set -e
DIR=$(dirname $0)
cd $DIR/../../

echo "-- Run 'Test' task --"

key="$1";
shift $(( $# > 0 ? 1 : 0  ))

case $key in
    debug)
        NODE_ENV=test \
        ./node_modules/.bin/mocha \
            --inspect \
            --debug-brk \
            --compilers js:babel-core/register \
            ./src/**/*.test.js
    ;;
    *)
        NODE_ENV=test \
        ./node_modules/.bin/mocha \
                --compilers js:babel-core/register \
                ./src/**/*.test.js
    ;;
esac
