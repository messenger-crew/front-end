#!/usr/bin/env sh
set -e

DIR=$(dirname $0)
cd $DIR/../../

echo "=== Run 'Dev-Server' task ==="

. ./.settings

FE__HOST=$FE__HOST \
FE__PORT=$FE__PORT \
FE__PATH_APP=$FE__PATH_APP \
FE__PATH_BUILD=$FE__PATH_BUILD \
FE__PATH_NODE_MODULES=$FE__PATH_NODE_MODULES \
NODE_ENV=development \
./node_modules/.bin/webpack-dev-server \
    --config tools/webpack/dev.config.js \
    --content-base src \
    --hot --inline \
    --colors \
    --history-api-fallback \
    --host $FE__HOST \
    --port $FE__PORT \
    --progress
