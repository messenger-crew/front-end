#!/usr/bin/env sh
set -e
DIR=$(dirname $0)
cd $DIR/../../

echo "-- Run 'jsdoc' task --"

./node_modules/.bin/jsdoc -c .jsdoc -R README.md ./src

echo 'Done.'
