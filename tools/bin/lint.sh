#!/usr/bin/env sh
set -e
DIR=$(dirname $0)
cd $DIR/../../

echo "-- Run 'Lint' task --"

./node_modules/.bin/eslint --format './node_modules/eslint-friendly-formatter' ./src

echo 'Done.'
