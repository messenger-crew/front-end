const path = require('path')
const webpack = require('webpack')
const SETTINGS = require('./settings')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CONFIG = require('./config')

CONFIG.entry.unshift('bootstrap-loader/extractStyles')
CONFIG.output = {
    path: SETTINGS.PATH.BUILD,
    filename: 'bundle-[hash].js'
}
CONFIG.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
        compress: { warnings: false },
        comments: false,
        sourceMap: true
    }),
    new ExtractTextPlugin({
        filename: 'bundle-[hash].css',
        allChunks: true
    }),
    new HtmlWebpackPlugin({
        template: path.resolve(SETTINGS.PATH.APP, 'index.html'),
        inject: true
    })
)
CONFIG.module.rules.push({
    test: /\.scss$/,
    use: ExtractTextPlugin.extract({
        fallback: 'style',
        use: ['css?root=.', '!postcss', '!sass']
    })
}, {
    test: /\.css$/,
    use: ExtractTextPlugin.extract({
        fallback: 'style',
        use: ['css?root=.', 'postcss']
    })
})
module.exports = CONFIG
