const path = require('path')
const webpack = require('webpack')
const SETTINGS = require('./settings.js')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CONFIG = require('./config')

CONFIG.devtool = 'inline-source-map'
CONFIG.entry.unshift(
    `webpack-hot-middleware/client?//${SETTINGS.HOST}:${SETTINGS.DEFAULT_PORT}&reload=true`,
    'webpack/hot/only-dev-server',
    'bootstrap-loader'
)
CONFIG.plugins.push(
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlWebpackPlugin({
        template: path.resolve(SETTINGS.PATH.APP, 'index.html'),
        inject: false,
        hash: true,
        webpackDevServerJs: '<script type="text/javascript" src="/bundle.js"></script>',
        stylesFile: ''
    })
)
CONFIG.module.rules.push({
    test: /\.scss$/,
    use: ['style', 'css?root=.', '!postcss', '!sass']
}, {
    test: /\.css$/,
    use: ['style', 'css?root=.', 'postcss']
})
module.exports = CONFIG
