const path = require('path')
const webpack = require('webpack')
const SETTINGS = require('./settings.js')

module.exports = {
    entry: [path.resolve(SETTINGS.PATH.APP, 'index.js')],
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV)
            }
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Tether: 'tether',
            'window.Tether': 'tether',
            Alert: 'exports-loader?Alert!bootstrap/js/dist/alert',
            Button: 'exports-loader?Button!bootstrap/js/dist/button',
            Carousel: 'exports-loader?Carousel!bootstrap/js/dist/carousel',
            Collapse: 'exports-loader?Collapse!bootstrap/js/dist/collapse',
            Dropdown: 'exports-loader?Dropdown!bootstrap/js/dist/dropdown',
            Modal: 'exports-loader?Modal!bootstrap/js/dist/modal',
            Popover: 'exports-loader?Popover!bootstrap/js/dist/popover',
            Scrollspy: 'exports-loader?Scrollspy!bootstrap/js/dist/scrollspy',
            Tab: 'exports-loader?Tab!bootstrap/js/dist/tab',
            Tooltip: 'exports-loader?Tooltip!bootstrap/js/dist/tooltip',
            Util: 'exports-loader?Util!bootstrap/js/dist/util'
        })
    ],
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /(node_modules)/,
            use: 'babel-loader'
        }, {
            test: /\.(png|jpe?g|gif)$/,
            use: 'url-loader'
        }, {
            test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
            use: 'file-loader'
        }, {
            test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
            use: `url-loader?prefix=font/&limit=${SETTINGS.CSS.BASE64_SIZE_LIMIT}`
        }, {
            test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
            use: `url-loader?limit=${SETTINGS.CSS.BASE64_SIZE_LIMIT}&mimetype=application/octet-stream`
        }, {
            test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
            use: `url-loader?limit=${SETTINGS.CSS.BASE64_SIZE_LIMIT}&mimetype=image/svg+xml`
        }, {
            test: /bootstrap[\/\\]js[\/\\]dist[\/\\]/,
            use: 'imports-loader?jQuery=jquery'
        }]
    }
}
