const path = require('path')

module.exports = {
    DEFAULT_PORT: process.env.FE__PORT || 8080,
    HOST: process.env.FE__HOST || '0.0.0.0',
    CSS: {
        BASE64_SIZE_LIMIT: 5000
    },
    PATH: {
        APP: path.resolve(__dirname, `../../${process.env.FE__PATH_APP || 'src'}`),
        BUILD: path.resolve(__dirname, `../../${process.env.FE__PATH_BUILD|| 'dist'}`),
        NODE_MODULES: path.resolve(__dirname, `../../${process.env.FE__PATH_NODE_MODULES | 'node_modules'}`)
    }
}
