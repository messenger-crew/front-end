#!/usr/bin/env sh
set -e
DIR=$(dirname $0)
cd $DIR/../../../
DIR=$(pwd)

# load config file
CONFIG_FILE=${CONFIG_FILE:-".settings"};
. $DIR/$CONFIG_FILE

# take variables
APP_NAME=$(tools/bin/config.sh name)
APP_VERSION=$(tools/bin/config.sh version)
IMAGE_NAME=$APP_NAME:$APP_VERSION
CONTAINER_NAME=$APP_NAME"-container"
PORT=${DOCKER_PRODUCTION__PORT:-8080};

helpCommand () {
    echo "Commands:"
    echo "    build: build docker image with application"
    echo "    run: run docker image with application"
    echo "    rm: remove docker container"
    echo "    rmi: remove docker image"
    echo "    help: show list of command"
}

build () {
    echo "Build docker image \""$IMAGE_NAME"\""
    if [ "$(is_build)" == yes ]; then
        echo "Docker image \""$IMAGE_NAME"\" already exists"
        exit
    fi
    CMD="docker build
                -f $DIR/$DOCKER_PRODUCTION__DOCKERFILE_PATH
                --tag $IMAGE_NAME ."
    echo $CMD
    $CMD
    echo "Docker image \""$IMAGE_NAME"\" was built"
}

run () {
    echo "Run container \""$CONTAINER_NAME"\" from the image \""$IMAGE_NAME"\""
    if [ "$(is_build)" == no ]; then
        echo "Docker image \""$IMAGE_NAME"\" is not built. You should build docker image before running"
        exit
    fi
    if [ "$(is_running)" == yes ]; then
        echo "Docker container \""$CONTAINER_NAME"\" is already run"
        exit
    fi
    CMD="docker run"
    if [ $1 == serve ]; then
        CMD=$CMD" -v "$DIR/dist:/usr/share/nginx/html
    fi
    CMD=$CMD" -p $PORT:80
            --rm
            -it
            --name $CONTAINER_NAME
            $IMAGE_NAME"
    echo $CMD
    $CMD
}

# remove functions
removeContainer () {
    $DIR/tools/docker/bin/tools.sh rm "$CONTAINER_NAME"
}

removeImage () {
    $DIR/tools/docker/bin/tools.sh rmi "$IMAGE_NAME"
}

# status functions
is_running () {
    $DIR/tools/docker/bin/tools.sh is_running "$CONTAINER_NAME"
}

is_build () {
    $DIR/tools/docker/bin/tools.sh is_build "$IMAGE_NAME"
}

status () {
    $DIR/tools/docker/bin/tools.sh status "$IMAGE_NAME" "$CONTAINER_NAME"
}

key="$1";

shift $(( $# > 0 ? 1 : 0  ))

echo " ==== DEV DOCKER ==== "

case $key in
    build)
        build
    ;;
    run)
        run
    ;;
    serve)
        run serve
    ;;
    rm)
        removeContainer
    ;;
    rmi)
        removeImage
    ;;
    help)
        helpCommand
    ;;
    *)
        echo "Wrong command..."
        helpCommand
    ;;
esac
