#!/usr/bin/env sh
set -e

is_build () {
    CMD="docker images -q -a --filter=reference=$1"
    IMAGE_ID=$($CMD)
    if [ -n "$IMAGE_ID" ]; then
        echo yes
    else
        echo no
    fi
}

is_running () {
    CONTAINER_ID=$(docker ps -q --all --filter "name=$1")
    if [ -n "$CONTAINER_ID" ]; then
        echo yes
    else
        echo no
    fi
}

status () {
    if [ "$(is_build "$1")" == no ]; then
        echo "Docker image \""$1"\" is not built"
    else
        echo "Docker image \""$1"\" is built"
    fi

    if [ "$(is_running "$2")" == no ]; then
        echo "Docker container \""$2"\" is not run"
    else
        echo "Docker container \""$2"\" is run"
    fi
}

# remove functions
removeContainer () {
    echo "Stop and remove container \""$1"\""
    #stop
    CMD_STOP="docker stop $1"
    echo $CMD_STOP
    $CMD_STOP
    #remove
    CMD_REMOVE="docker rm $1"
    echo $CMD_REMOVE
    $CMD_REMOVE
}

removeImage () {
    echo "Remove image \""$1"\""
    CMD="docker rmi $1"
    echo $CMD
    $CMD
}

key="$1";

shift $(( $# > 0 ? 1 : 0  ))

case $key in
    is_build)
        is_build "$1"
    ;;
    is_running)
        is_running "$1"
    ;;
    rm)
        removeContainer "$1"
    ;;
    rmi)
        removeImage "$1"
    ;;
    status)
        status "$1" "$2"
    ;;
    *)
        echo "Wrong command"
    ;;
esac
