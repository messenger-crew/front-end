In the first time you should run this script from the root folder for installing all dependencies that are needed for a project.
```shell
./tools/bin/init-app.sh
```

## NPM commands:
- ``npm start`` - to launch a dev server on your source files
- ``npm test`` - to launch unit tests
- ``npm run lint`` - to launch eslint tests
- ``npm run jsdoc`` - to launch generation of the jsdoc documentation
- ``npm run build`` - eslint + test + build
- ``npm run deploy`` - build project and put them to the docker image
- ``npm run deploy:serve`` - run docker image with files that contains in the dist folder

### Build project
For building you should run this command `npm run deploy`.
This command has these steps:

* Deleting temporary files
* Running eslint
* Running unit tests
* Compiling js files and styles and putting result to the `./dist/`
* Creating docker image

### Running tests and creating reference screenshots
You will need to execute following command twice, for any new tests you have been added.
```shell
npm run ui-test
```
The first run will create a screenshot which will be referenced as a correct image for the component.
To run existing tests only, you can use following command:
```shell
npm run ui-test:only
```

### Production
```shell
npm run production [command]
```
##### Commands:
Command       | Description
------------- | -------------
status        | show status
build         | build docker image with application
run           | run docker image with application
rm            | remove docker container
rmi           | remove docker image
help          | show list of command
