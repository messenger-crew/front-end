export default {
    isInDevMode: process.env.NODE_ENV === 'development',
    devTools: {
        toggleVisibilityKey: 'ctrl-q',
        changePositionKey: 'ctrl-p',
        defaultPosition: 'bottom'
    },
    ajax: {
        host: '//localhost:5123/api/v2'
    }
}
