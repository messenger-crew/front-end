/** @function
 * @name composeReducers
 * @description Helper function that composes reducers to the one reducer.
 * @param {array} fns array of the reducers.
 * @returns {function} return one reducer.
 */
export default function composeReducers(...fns) {
    return function composeReducersWrapper(state, action) {
        for (let i = fns.length - 1; i >= 0; i--) {
            state = fns[i](state, action)
        }
        return state
    }
}
