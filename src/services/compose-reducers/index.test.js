import assert from 'assert'
import composeReducers from './index'

describe('composeReducers.js', () => {
    it('should be a function', () => {
        assert.equal(typeof composeReducers, 'function')
    })

    it('should return a function', () => {
        assert.equal(typeof composeReducers(), 'function')
    })

    describe('should change state', () => {
        let reducer1 = null
        let reducer2 = null
        let composedReducer = null

        beforeEach(() => {
            reducer1 = function(state, action) {
                if ('SET_STATE/reducer1' === action.type || 'SET_STATE' === action.type) {
                    return { value: state.value + 1 }
                }
                return state
            }
            reducer2 = function(state, action) {
                if ('SET_STATE/reducer2' === action.type || 'SET_STATE' === action.type) {
                    return { value: state.value + 2 }
                }
                return state
            }
            composedReducer = composeReducers(reducer2, reducer1)
        })

        it('should equal { value: 1 }', () => {
            const result = composedReducer({ value: 1 }, { type: 'SET_STATE/reducer3' })
            assert.deepEqual(result, { value: 1 })
        })

        it('should equal { value: 2 }', () => {
            const result = composedReducer({ value: 1 }, { type: 'SET_STATE/reducer1' })
            assert.deepEqual(result, { value: 2 })
        })

        it('should equal { value: 3 }', () => {
            const result = composedReducer({ value: 1 }, { type: 'SET_STATE/reducer2' })
            assert.deepEqual(result, { value: 3 })
        })

        it('should equal { value: 4 }', () => {
            const result = composedReducer({ value: 1 }, { type: 'SET_STATE' })
            assert.deepEqual(result, { value: 4 })
        })
    })
})
