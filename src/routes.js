import React from 'react'
import { Route, Switch } from 'react-router-dom'
import AuthRoute from './containers/auth-route'
import HomePage from './components/home-page'
import ChildPages from './components/child-pages'
import LoginPage from './containers/login-page'
import Page404 from './components/page-404'
import GridListPage from './containers/grid-list-page'

export default (
    <Switch>
        <Route path="/login" component={LoginPage} />
        <Route path="/gridlist" component={GridListPage} />
        <AuthRoute to="/login" path="/">
            <Switch>
                <Route exact path="/" component={HomePage} />
                <Route path="/childPages" component={ChildPages} />
                <Route component={Page404} />
            </Switch>
        </AuthRoute>
    </Switch>
)
