import React from 'react'
import { createDevTools } from 'redux-devtools'
import LogMonitor from 'redux-devtools-log-monitor'
import DockMonitor from 'redux-devtools-dock-monitor'
import * as Router from './create-router'
import createStore from '../store'

const createAppDevTools = function(config) {
    return createDevTools(
        <DockMonitor
            toggleVisibilityKey={config.devTools.toggleVisibilityKey}
            changePositionKey={config.devTools.changePositionKey}
            defaultPosition={config.devTools.defaultPosition}
        >
            <LogMonitor theme="tomorrow" preserveScrollTop={false} />
        </DockMonitor>
    )
}

export default function createReduxResources(config) {
    const DevTools = config.isInDevMode ? createAppDevTools(config) : null
    const store = createStore({
        config,
        reducers: { routing: Router.createReducer() },
        middlewares: [ Router.createMiddleware() ],
        instruments: config.isInDevMode ? [DevTools.instrument()] : []
    })
    const reduxHistory = Router.createHistory(store)
    return { DevTools, reduxHistory, store }
}
