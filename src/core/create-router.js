import { syncHistoryWithStore } from 'react-router-redux'
import createBrowserHistory from 'history/createBrowserHistory'
import { routerMiddleware } from 'react-router-redux'
import { LOCATION_CHANGE } from 'react-router-redux'
import Immutable from 'immutable'

const history = createBrowserHistory()

export const createMiddleware = () => routerMiddleware(history)

export const createReducer = function() {
    const initState = new Immutable.Map({ locationBeforeTransitions: null })
    return function routerReducer(state = initState, {type, payload}) {
        if (type === LOCATION_CHANGE) {
            return state.set('locationBeforeTransitions', payload)
        }
        return state
    }
}

export const createHistory = function(store) {
    return syncHistoryWithStore(history, store, {
        selectLocationState: (state) => state.get('routing').toObject()
    })
}
