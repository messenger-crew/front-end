import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import createReduxResources from './create-redux-resources'

export default class Core {
    constructor(config, routes) {
        this._routes = this.setRoutes(routes)
        this._config = this.setConfig(config)
    }

    setConfig(config = null) {
        this._config = Object.assign({
            isInDevMode: false
        }, config)
        return this
    }

    setRoutes(routes = null) {
        this._routes = routes
        return this
    }

    renderTo(domContainer) {
        const resources = createReduxResources(this._config)
        const DevTools = resources.DevTools
        const appDOM = (
            <Provider store={resources.store}>
                <BrowserRouter history={resources.reduxHistory}>
                    <div className="app_reactroot">
                        { this._routes }
                        { this._config.isInDevMode && <DevTools /> }
                    </div>
                </BrowserRouter>
            </Provider>
        )
        ReactDOM.render(appDOM, domContainer)
        return this
    }
}
