import React from 'react'
import { connect } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'
import { SELECTORS } from '../../store'

class AuthRoute extends React.Component {
    isRedirect(data) {
        return !data
    }

    renderBody(component, to, user, children, props) {
        if (this.isRedirect(user)) {
            return (<Redirect from={props.path} to={to} />)
        }
        if (component) {
            return React.createElement(component, props, children)
        }
        return (children)
    }

    render() {
        const { component, to, user, children, ...props } = this.props
        return (<Route {...props} render={() => this.renderBody(component, to, user, children, props)} />)
    }
}

const mapStateToProps = (state) => ({ user: SELECTORS.auth.getUsername(state) })

export default connect(mapStateToProps)(AuthRoute)
