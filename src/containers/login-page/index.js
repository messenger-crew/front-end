import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { SELECTORS, ACTIONS } from '../../store'

const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const PASSWORD_REGEX = /^[A-Za-z0-8]{8,}$/

class LoginPage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            showError: false
        }
        this.onClick = this._onClick.bind(this)
        this.onChangeEmail = this._onChangeInput.bind(this, 'email')
        this.onChangePassword = this._onChangeInput.bind(this, 'password')
    }

    _onClick(event) {
        event.preventDefault()
        if (EMAIL_REGEX.test(this.state.email) && PASSWORD_REGEX.test(this.state.password)) {
            this.props.authUser(this.state.email, this.state.password)
            return
        }
        this.setState({ showError: true })
    }

    _onChangeInput(property, event) {
        let newState = {}
        newState[property] = event.target.value
        this.setState(newState)
    }

    render() {
        if (this.props.user) {
            return (<Redirect to="/childPages" />)
        }
        return (
            <div className="login">
                <h1 className="title">Sign in</h1>
                <div className={this.state.showError ? 'error' : 'hidden'} > Something go wrong </div>
                <form className="bd-example">
                   <div className="form-group row">
                        <label htmlFor="email" className="col-2 col-form-label">Email</label>
                        <div className="col-10">
                            <input className="form-control"
                                    type="email"
                                    id="email"
                                    onChange={this.onChangeEmail}
                                    value={this.state.email}/>
                        </div>
                    </div>
                   <div className="form-group row">
                        <label htmlFor="password" className="col-2 col-form-label">Password</label>
                        <div className="col-10">
                            <input className="form-control"
                                    autoComplete="new-password"
                                    type="password"
                                    id="password"
                                    onChange={this.onChangePassword}
                                    value={this.state.password}/>
                        </div>
                    </div>
                   <div className="form-group">
                        <button className="sign-in btn btn-primary"
                                onClick={this.onClick}>Sign in</button>
                    </div>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({ user: SELECTORS.auth.getUsername(state) })

const mapDispatchToProps = { authUser: ACTIONS.auth.request }

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)
