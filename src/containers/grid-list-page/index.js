import React from 'react'
import { connect } from 'react-redux'
import {
    GridList, GLHeader, GLBody, GLRow, GLCell, GLCellNumber,
    GLCellLink, GLCellSort, GBCellSlider, GLCellCheckbox, GLCellContextMenu
} from '../../components/grid-list'
import { SELECTORS, ACTIONS } from '../../store'
import { Menu, MItem } from '../../components/menu'

class GridListPage extends React.Component {
    constructor(props) {
        super(props)
        this.sortGrid = this._sortGrid.bind(this)
    }

    componentDidMount() {
        this.props.loadDataClients()
    }

    _sortGrid(sort) {
        this.props.sortData(sort)
    }

    render() {
        return (
            <div className="grid-list-page">
                <h1>GridListPage</h1>
                <GridList>
                    <GLHeader>
                        <GLRow>
                            <GLCell>#</GLCell>
                            <GLCell>Number</GLCell>
                            <GLCell>Selected</GLCell>
                            <GLCellSort type={this.props.sort} onClick={this.sortGrid}>Name</GLCellSort>
                            <GLCell>Gender</GLCell>
                            <GLCell>Company</GLCell>
                            <GLCell>Volume</GLCell>
                            <GLCell>Email</GLCell>
                        </GLRow>
                    </GLHeader>
                    <GLBody>
                        {this.props.clientsList.map((item, index) => (
                            <GLRow key={index}>
                                <GLCell>{index + 1}</GLCell>
                                <GLCellNumber>{index + 1}</GLCellNumber>
                                <GLCellCheckbox />
                                <GLCellContextMenu label={item.name}>
                                    <Menu>
                                        <MItem>Option 1</MItem>
                                        <MItem>Option 2</MItem>
                                        <MItem>Option 3</MItem>
                                    </Menu>
                                </GLCellContextMenu>
                                <GLCell>{item.gender}</GLCell>
                                <GLCell>{item.company}</GLCell>
                                <GBCellSlider>{30}</GBCellSlider>
                                <GLCellLink type="email">{item.email}</GLCellLink>
                            </GLRow>
                        ))}
                    </GLBody>
                </GridList>
            </div>
        )
    }
}

GridListPage.displayName = 'GridListPage'

const mapStateToProps = (state) => ({
    clientsList: SELECTORS.gridListPage.getDataClientsList(state),
    sort: SELECTORS.gridListPage.getSort(state)
})

const mapDispatchToProps = {
    loadDataClients: ACTIONS.dataClients.loadData,
    sortData: ACTIONS.gridListPage.sortData
}

export default connect(mapStateToProps, mapDispatchToProps)(GridListPage)
