import React from 'react'
import { connect } from 'react-redux'
import { SELECTORS } from '../../store'

class Test1Page extends React.Component {
    render() {
        return (
            <div className="test-sub-page">
                <h3>Test 1</h3>
                <p>
                    <b>username: </b>
                    {this.props.username ? this.props.username : 'none'}
                </p>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({ username: SELECTORS.auth.getUsername(state) })

export default connect(mapStateToProps)(Test1Page)
