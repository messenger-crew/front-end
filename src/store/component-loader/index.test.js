import _ from 'lodash'
import assert from 'assert'
import sinon from 'sinon'
import ComponentLoader from './index'

describe('component-loader', () => {
    describe('contructor', () => {
        let cl = null
        beforeEach(() => cl = new ComponentLoader())
        it('should be instance of ComponentLoader', () => assert.ok(cl instanceof ComponentLoader))
        it('modules is object', () => assert.ok(_.isObject(cl._modules)))
        it('module should equal { reducers: [], middlewares[] }', () => assert.deepEqual(cl._modules, { reducers: [], middlewares: [] }))
        it('set() should be a function', () => assert.equal(typeof cl.set, 'function'))
        it('clear() should be a function', () => assert.equal(typeof cl.clear, 'function'))
        it('getReducers() should be a function', () => assert.equal(typeof cl.getReducers, 'function'))
        it('setReducers() should be a function', () => assert.equal(typeof cl.setReducers, 'function'))
        it('getMiddlewares() should be a function', () => assert.equal(typeof cl.getMiddlewares, 'function'))
        it('setMiddlewares() should be a function', () => assert.equal(typeof cl.setMiddlewares, 'function'))
        it('executeInitModuleFunctions() should be a function', () => assert.equal(typeof cl.executeInitModuleFunctions, 'function'))
    })

    describe('executeInitModuleFunctions()', () => {
        let spies = null
        beforeEach(() => {
            spies = { reducers: sinon.spy(), middlewares: sinon.spy() }
            new ComponentLoader({
                reducers: [{ module1: { initModule: spies.reducers } }],
                middlewares: [{ module1: { initModule: spies.middlewares }, module2: { initModule: spies.middlewares } }]
            }).executeInitModuleFunctions('test')
        })

        it('should execute all initModule functions', () => {
            assert.equal(spies.reducers.callCount, 1)
            assert.equal(spies.reducers.args[0].length, 1)

            assert.equal(spies.middlewares.callCount, 2)
            assert.equal(spies.middlewares.args[0].length, 1)
            assert.equal(spies.middlewares.args[1].length, 1)
        })
    })

    describe('getReducers()', () => {
        let spies = null
        let cl = null
        const getStores = () => Object.keys(cl.getReducers()).sort((a, b) => (a === b) ? 0 : (a < b ? -1 : 1))
        const shouldEqual = function(key, value) {
            const reducers = cl.getReducers()
            reducers[key]()
            assert.equal(spies[key].callCount, value)
            const zeroCallsStore = ['store1', 'store2', 'store3', 'store4']
            zeroCallsStore.splice(zeroCallsStore.indexOf(key), 1)
            zeroCallsStore.forEach((key) => assert.equal(spies[key].callCount, 0))
        }

        beforeEach(() => {
            spies = { store1: sinon.spy(), store2: sinon.spy(), store3: sinon.spy(), store4: sinon.spy() }
            cl = new ComponentLoader({
                reducers: [{
                    module1: { store: 'store1', reducer: spies.store1 },
                    module2: { store: 'store2', reducer: spies.store2 }
                }, {
                    module1: { store: 'store1', reducer: spies.store1 },
                    module2: { store: 'store3', reducer: spies.store3 },
                    module3: {}
                }],
                middlewares: [{
                    module1: { store: 'store3', reducer: spies.store3 },
                    module2: { store: 'store4', reducer: spies.store4 }
                }]
            })
        })

        it('should contain these keys "store1", "store2:", "store3", "store4"', () => assert.deepEqual(getStores(cl.getReducers()), ['store1', 'store2', 'store3']))
        it('spies.store1.callCount should equal 2', () => shouldEqual('store1', 2))
        it('spies.store2.callCount should equal 1', () => shouldEqual('store2', 1))
        it('spies.store3.callCount should equal 1', () => shouldEqual('store3', 1))
    })

    describe('getMiddleware()', () => {
        let cl = null
        let spies = null

        beforeEach(() => {
            spies = { other: () => {}, middlewares: () => {} }
            cl = new ComponentLoader({
                reducers: [{ module1: { middleware: spies.other } }],
                middlewares: [{
                    module1: { middleware: [spies.middlewares, spies.middlewares] },
                    module2: { middleware: spies.middlewares },
                    module3: {}
                }, {
                    module1: { middleware: spies.middlewares },
                    module2: {}
                }]
            })
        })

        it('should return middlewares from the modules with type that equal middlewares', () => {
            const middleware = cl.getMiddlewares()
            assert.equal(middleware.length, 4)
            middleware.forEach((middleware) => assert.equal(middleware, spies.middlewares))
        })
    })
})
