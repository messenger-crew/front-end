import _ from 'lodash'
import composeReducers from '../../services/compose-reducers'

//region execute initModule function
const executeInitModule = _.curry(function(Core, modules, name) {
    const module = modules[name]
    if (module.initModule) {
        module.initModule(Core)
    }
})

const reduceExecuteInitModule = _.curry(function(Core, module) {
    Object
        .keys(module)
        .forEach(executeInitModule(Core, module))
})
//endregion

//region middleware function
const addMiddlewareFromModule = _.curry(function(modules, middlewares, name) {
    const module = modules[name]
    if (!module.middleware) {
        return middlewares
    }
    if (Array.isArray(module.middleware)) {
        return [...middlewares, ...module.middleware]
    }
    middlewares.push(module.middleware)
    return middlewares
})

const getMiddlewareFromModules = function(middleware = [], modules) {
    const addMiddleware = addMiddlewareFromModule(modules)
    const modulesMiddleware = Object.keys(modules).reduce(addMiddleware, [])
    return [...middleware, ...modulesMiddleware]
}
//endregion

//region reducers function
const addReducersFromModule = _.curry(function(modules, reducers, name) {
    const module = modules[name]
    if (!module.reducer || !module.store) {
        return reducers
    }
    if (!reducers[module.store]) {
        reducers[module.store] = module.reducer
        return reducers
    }
    reducers[module.store] = composeReducers(
        module.reducer,
        reducers[module.store]
    )
    return reducers
})

const getReducersFromModules = function(initialValue = {}, modules) {
    return Object
        .keys(modules)
        .reduce(addReducersFromModule(modules), initialValue)
}
//endregion

const joinModules = function(modules = {}) {
    return Object
            .keys(modules)
            .reduce((container, key) => [...container, ...modules[key]], [])
}

export default class ComponentLoader {
    constructor(modules = {}) {
        this.set(modules)
    }

    clear() {
        return this.set()
    }

    set(modules = {}) {
        this._modules = Object.assign({
            reducers: [],
            middlewares: []
        }, modules)
        return this
    }

    setReducers(reducers = []) {
        this._modules.reducers = reducers
        return this
    }

    setMiddlewares(middlewares = []) {
        this._modules.middlewares = middlewares
        return this
    }

    executeInitModuleFunctions(...args) {
        joinModules(this._modules).forEach(reduceExecuteInitModule(...args))
        return this
    }

    getMiddlewares() {
        return this._modules.middlewares.reduce(getMiddlewareFromModules, [])
    }

    getReducers(initialRootReducer = {}) {
        return this._modules.reducers.reduce(getReducersFromModules, initialRootReducer)
    }
}
