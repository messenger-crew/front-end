import { createSelector } from 'reselect'

export default createSelector(
    (state) => (state.get('auth') && state.get('auth').get('user')) || null,
    (userData) => (userData && userData.username) || null
)
