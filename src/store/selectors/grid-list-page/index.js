import getDataClientsList from './get-data-clients-list'
import getSort from './get-sort'

export default {
    getDataClientsList,
    getSort
}
