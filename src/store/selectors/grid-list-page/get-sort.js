import { createSelector } from 'reselect'
import * as constants from '../../reducers/grid-list-page/constants'

export default createSelector(
    (state) => state.get(constants.STORE).get('sort'),
    (sort) => sort
)
