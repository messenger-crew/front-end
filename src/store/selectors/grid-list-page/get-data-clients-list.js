import { createSelector } from 'reselect'
import * as constants from '../../reducers/grid-list-page/constants'

function compareDesc(item1, item2) {
    if (item1.name === item2.name) {
        return 0
    }
    return item1.name < item2.name ? -1 : 1
}

function compareAsc(item1, item2) {
    return -1 * compareDesc(item1, item2)
}

function sortCLientsList(list, sort) {
    let sortFunction = sort === constants.SORT.ASC ? compareAsc : compareDesc
    return list.sort(sortFunction)
}

export default createSelector(
    (state) => state.get(constants.STORE_DATA_CLIENTS).get('list'),
    (state) => state.get(constants.STORE).get('sort'),
    (list, sort) => !list ? [] : sortCLientsList(list.toArray(), sort)
)
