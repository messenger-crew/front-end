import auth from './auth'
import gridListPage from './grid-list-page'

export default {
    auth,
    gridListPage
}
