import ajax from './ajax'
import socket from './socket'

export default {
    ajax,
    socket
}
