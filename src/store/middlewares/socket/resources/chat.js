//actions
export const actionTypes = {
    CHAT_ADD_MESSAGE: 'RESOURCES_CHAT/ADD_MESSAGE',
    CHAT_SEND_MESSAGE: 'RESOURCES_CHAT/SEND_MESSAGE'
}

const RESOURCE_NAME = 'msg'

const addMessage = (message, channel) => ({
    type: actionTypes.CHAT_ADD_MESSAGE,
    message,
    channel
})

const request = function(data) {
    if ('send' !== data.method) {
        return null
    }
    return addMessage(data.message.value, data.message.channel)
}

const response = function(action) {
    switch (action.type) {
        case actionTypes.CHAT_SEND_MESSAGE: {
            return {
                resource: RESOURCE_NAME,
                method: 'send',
                message: {
                    channel: action.channel,
                    value: action.message
                }
            }
        }
    }
    return null
}

export default {
    name: RESOURCE_NAME,
    response,
    request
}
