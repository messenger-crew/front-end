class Socket {
    constructor(...args) {
        this._listeners = {
            onOpen: () => {},
            onInit: () => {},
            onClose: () => {},
            onError: () => {},
            onMessage: () => {},
            onDispatch: () => {}
        }
        this.init(...args)
    }

    init(opt = {}) {
        this._config = opt.config || null
        if (!opt.listeners) {
            this._listeners.onInit()
            return this
        }
        for (let key in this._listeners) {
            if (opt.listeners[key]) {
                this._listeners[key] = opt.listeners[key]
            }
        }
        this._listeners.onInit()
        return this
    }

    dispatch(action) {
        const message = this._listeners.onDispatch(action)
        if (message) {
            this.send(JSON.stringify(message))
        }
        return this
    }

    start() {
        if (this._socket || !this._config) {
            return this
        }
        this._socket = new WebSocket(this._config.url)
        this._socket.addEventListener('open', (event) => this._listeners.onOpen(event))
        this._socket.addEventListener('close', (event) => {
            this._socket = null
            this._listeners.onClose(event)
        })
        this._socket.addEventListener('message', (event) => this._listeners.onMessage(event))
        this._socket.addEventListener('error', (event) => this._listeners.onError(event))
        return this
    }

    send(message) {
        this._socket && this._socket.send(message)
        return this
    }

    close() {
        this._socket && this._socket.close()
        return this
    }
}

export default new Socket()
