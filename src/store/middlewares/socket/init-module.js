import _ from 'lodash'
import * as actions from '../../actions/socket'
import socket from './socket'
import resources from './resources'
import DebounceService from './debounce-service'

const onMessageCreator = _.curry(function(store, resources, dService, event) {
    const data = JSON.parse(event.data)
    const resource = resources.find((el) => el.name === data.resource)
    if (!resource) {
        return
    }
    const action = resource.request(data)
    if (action) {
        dService.execute(action)
    }
})

const onDispatchCreator = _.curry((resources, action) => {
    for (let i = 0, l = resources.length; i < l; i++) {
        let response = resources[i].response(action)
        if (response) {
            return response
        }
    }
    return null
})
const createListener = (store, type) => (data) => store.dispatch({ type: type, data: data || null })

const dispatch = _.curry(function(store, actions) {
    const actionsByType = actions.reduce(function getMessage(actions, action) {
        if (0 === action.length) {
            return actions
        }
        if (!actions.hasOwnProperty(action[0].type)) {
            actions[action[0].type] = {
                type: action[0].type,
                debounced: true,
                data: []
            }
        }
        actions[action[0].type].data.push(action[0])
        return actions
    }, {})
    Object.keys(actionsByType).forEach(function dispatchActions(name) {
        store.dispatch(actionsByType[name])
    })
})

export default function initSocket(core) {
    const dService = new DebounceService(dispatch(core.store), {
        delay: 200
    })
    socket.init({
        config: core.config.ws,
        listeners: {
            onMessage: onMessageCreator(core.store, resources, dService),
            onDispatch: onDispatchCreator(resources),
            onInit: createListener(core.store, actions.TYPES.SOCKET_INIT),
            onOpen: createListener(core.store, actions.TYPES.SOCKET_START),
            onClose: createListener(core.store, actions.TYPES.SOCKET_CLOSE),
            onError: createListener(core.store, actions.TYPES.SOCKET_ERROR),
        }
    }).start()
}
