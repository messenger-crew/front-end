import socket from './socket'

export default () => next => action => {
    socket.dispatch(action.action)
    return next(action)
}
