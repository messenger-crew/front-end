import _ from 'lodash'

class DebounceService {
    constructor(handler = () => {}, conf = {}) {
        this.__queue = []
        this.__handler = handler
        this.__moveToHandlerDebounced = _.debounce(
            this._moveToHandler.bind(this), conf.delay || undefined
        )
    }

    execute(...args) {
        this.__queue.push(args)
        this.__moveToHandlerDebounced()
        return this
    }

    _moveToHandler() {
        this.__handler && this.__handler([...this.__queue])
        this.__queue.length = 0
        return this
    }
}

export default DebounceService
