import { AJAX_METHODS } from './constants'
import _ from 'lodash'

class Ajax {
    constructor(...args) {
        this.init(...args)
    }

    init(config = {}, store = null) {
        this._config = config
        this._store = store
        return this
    }

    prepareParams(request) {
        if (AJAX_METHODS.GET === request.method) {
            const searchParams = new URLSearchParams()
            Object.keys(request.params).forEach((key) => {
                let value = request.params[key]
                if (null === value) {
                    return
                }
                if (_.isObject(request.params[key])) {
                    value = JSON.parse(value)
                }
                searchParams.set(key, value)
            })
            request.url += '?' + searchParams.toString()
        }
        return request
    }

    fetch(request) {
        if (!request) {
            return this
        }
        request.url = this._config.host + request.url
        if (!request.method) {
            request.method = AJAX_METHODS.GET
        }
        if (request.params) {
            request = this.prepareParams(request)
        }
        return fetch(request.url, {
            method: request.method
        }).catch((data) => {
            if (this._config.isInDevMode && request.stub) {
                return Promise.resolve(request.stub(request))
            }
            return data
        })
    }
}

export default new Ajax()
