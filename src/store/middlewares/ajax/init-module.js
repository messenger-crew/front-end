import ajax from './ajax'

export default function initSocket(core) {
    ajax.init({
        host: core.config.ajax.host,
        isInDevMode: core.config.isInDevMode
    }, core.store)
}
