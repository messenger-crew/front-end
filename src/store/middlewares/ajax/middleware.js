import ajax from './ajax'
import resourceList from './resources'

export default () => next => action => {
    let currentAct = action
    if (currentAct.action) {
        currentAct = currentAct.action
    }
    const resources = resourceList.filter((res) => -1 !== res.actions.indexOf(currentAct.type))
    if (!resources.length) {
        return next(action)
    }
    resources.forEach((resource) => {
        const request = resource.request(currentAct)
        if (!request) {
            return
        }
        ajax
            .fetch(request)
            .then((data) => resource.response(null, data, request))
            .catch((error, data) => resource.response(error, data, request))
            .then((action) => {
                if (action) {
                    ajax._store.dispatch(action)
                }
            })
    })

    return next(action)
}
