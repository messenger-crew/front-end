import stub from './stub/auth'
import actions from '../../../actions'
import { AJAX_METHODS } from '../constants'

const RESOURCE_NAME = 'auth'

const response = function(error, data, request) {
    if (error) {
        return null
    }
    return actions.auth.response(JSON.parse(data).data, request)
}

const stubHandler = () => JSON.stringify(Object.assign({}, stub))

const request = function(action) {
    switch (action.type) {
        case actions.auth.TYPES.AUTH_REQUEST: {
            let email = encodeURIComponent(action.email)
            let password = encodeURIComponent(action.password)
            return {
                url: `/auth?email=${email}&password=${password}`,
                method: AJAX_METHODS.GET,
                stub: stubHandler
            }
        }
    }
    return null
}

export default {
    actions: [actions.auth.TYPES.AUTH_REQUEST],
    name: RESOURCE_NAME,
    response,
    request
}
