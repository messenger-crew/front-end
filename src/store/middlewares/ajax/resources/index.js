import dataClients from './data-clients'
import auth from './auth'

export default [ dataClients, auth ]
