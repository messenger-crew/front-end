import stub from './stub/data-clients'
import { AJAX_METHODS } from '../constants'
import actions from '../../../actions'

const RESOURCE_NAME = 'dataClients'
const actionTypes = actions.dataClients.TYPES

const response = function(error, data, request) {
    if (error) {
        return null
    }
    return actions.dataClients.dataRequest(JSON.parse(data).data, request)
}

const stubHandler = () => JSON.stringify(Object.assign({}, stub))

const request = function(action) {
    switch (action.type) {
        case actionTypes.DATA_CLIENTS_REQUEST: {
            let opt = {
                url: '/gridlist',
                method: AJAX_METHODS.GET,
                stub: stubHandler
            }
            return opt
        }
    }
    return null
}

export default {
    actions: [actionTypes.DATA_CLIENTS_REQUEST],
    name: RESOURCE_NAME,
    response,
    request
}
