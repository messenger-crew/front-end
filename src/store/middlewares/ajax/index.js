import 'whatwg-fetch'
import initModule from './init-module'
import middleware from './middleware'

export default {
    initModule,
    middleware
}
