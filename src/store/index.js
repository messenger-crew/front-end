import { createStore, applyMiddleware, compose } from 'redux'
import { combineReducers } from 'redux-immutable'
import ComponentLoader from './component-loader'
import middlewares from './middlewares'
import reducers from './reducers'
import selectors from './selectors'
import actions from './actions'

const MODULES = {
    reducers: [reducers],
    middlewares: [middlewares]
}

//region utils
const pushFromTo = function(fromArr = [], toArr = []) {
    fromArr.forEach((item) => toArr.push(item))
    return toArr
}

const createReduxStore = function(reducers, middlewares = [], instruments = []) {
    return createStore(
        reducers,
        compose(
            applyMiddleware(...middlewares),
            ...instruments
        )
    )
}

const getReducers = (cl, reducers = {}) => combineReducers(cl.getReducers(reducers))
const getMiddlewares = (cl, middlewares = []) => pushFromTo(middlewares, cl.getMiddlewares())
//endregion

export const SELECTORS = selectors
export const ACTIONS = actions

export default function createAppStore(opt) {
    const cl = new ComponentLoader()
                    .setReducers(MODULES.reducers)
                    .setMiddlewares(MODULES.middlewares)
    const reducers = getReducers(cl, opt.reducers)
    const middlewares = getMiddlewares(cl, opt.middlewares)
    const store = createReduxStore(reducers, middlewares, opt.instruments || [])
    cl.executeInitModuleFunctions({ store, storeComponentLoader: cl, config: opt.config || {} })
    return store
}
