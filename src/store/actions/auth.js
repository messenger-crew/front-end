export const TYPES = {
    AUTH_REQUEST: 'AUTH/REQUEST',
    AUTH_RESPONSE: 'AUTH/RESPONSE'
}

export const request = (email, password) => ({
    type: TYPES.AUTH_REQUEST,
    email,
    password
})

export const response = (data) => ({
    type: TYPES.AUTH_RESPONSE,
    data
})
