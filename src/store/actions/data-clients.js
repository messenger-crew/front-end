export const TYPES = {
    DATA_CLIENTS_RESPONSE: 'DATA_CLIENTS/RESPONSE',
    DATA_CLIENTS_REQUEST: 'DATA_CLIENTS/REQUEST'
}

export const dataRequest = (list) => ({
    type: TYPES.DATA_CLIENTS_RESPONSE,
    list
})

export const loadData = () => ({
    type: TYPES.DATA_CLIENTS_REQUEST
})
