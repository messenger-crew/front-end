export const TYPES = {
    GRID_LIST_PAGE_SORT_DATA: 'GRID_LIST_PAGE/SORT_DATA'
}

export const sortData = (sort) => ({
    type: TYPES.GRID_LIST_PAGE_SORT_DATA,
    sort
})
