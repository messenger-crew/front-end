import * as auth from './auth'
import * as dataClients from './data-clients'
import * as gridListPage from './grid-list-page'
import * as socket from './socket'

export default {
    auth,
    dataClients,
    gridListPage,
    socket
}
