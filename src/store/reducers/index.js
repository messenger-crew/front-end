import dataClients from './data-clients'
import gridListPage from './grid-list-page'
import auth from './auth'

export default {
    auth,
    dataClients,
    gridListPage
}
