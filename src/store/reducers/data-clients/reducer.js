import initState from './init-state'
import actions from '../../actions'
import { List } from 'immutable'

export default function(state = initState, action) {
    switch (action.type) {
        case actions.dataClients.TYPES.DATA_CLIENTS_RESPONSE: {
            return state.set('list', new List(action.list))
        }
    }
    return state
}
