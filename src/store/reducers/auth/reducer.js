import initState from './init-state'
import actions from '../../actions'

export default function reducer(state = initState, action) {
    switch (action.type) {
        case actions.auth.TYPES.AUTH_RESPONSE:
            return state.set('user', action.data)
    }
    return state
}
