export const STORE = 'GridListPage'
export const STORE_DATA_CLIENTS = 'dataClients'
export const SORT = {
    ASC: 'asc',
    DESC: 'desc'
}
