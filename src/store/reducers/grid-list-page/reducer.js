import initState from './init-state'
import actions from '../../actions'

export default function(state = initState, action) {
    switch (action.type) {
        case actions.gridListPage.TYPES.GRID_LIST_PAGE_SORT_DATA: {
            return state.set('sort', action.sort)
        }
    }
    return state
}
