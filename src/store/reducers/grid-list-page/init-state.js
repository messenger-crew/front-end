import { Map } from 'immutable'
import * as constants from './constants'

export default new Map({
    sort: constants.SORT.DESC
})
