import * as constants from './constants'
import reducer from './reducer'

export default {
    store: constants.STORE,
    reducer
}
