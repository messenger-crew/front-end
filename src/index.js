import Core from './core'
import config from './config'
import routes from './routes'

new Core()
    .setConfig(config)
    .setRoutes(routes)
    .renderTo(document.getElementById('app'))
