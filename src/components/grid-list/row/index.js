import React, { Component } from 'react'

/** @class
 * @name GLRow
 * @description grid list row component
 */
class GLRow extends Component {
    render() {
        return (
            <div className='grid-row'>
                {this.props.children}
            </div>
        )
    }
}

GLRow.displayName = 'GLRow'

export default GLRow
