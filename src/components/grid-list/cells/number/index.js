import React, { Component, PropTypes } from 'react'
import GLCell from '../default'

/** @class
 * @name GLCellNumber
 * @description grid cell number component
 */
class GLCellNumber extends Component {
    constructor(props) {
        super(props)
        this.onChange = this._onChange.bind(this)
    }

    _onChange(event) {
        this.props.onChange(event.target.value)
    }

    render() {
        const { className, children, ...rest } = this.props
        return (
            <GLCell className={`cell-number ${className}`} {...rest}>
                <input type="number"
                        value={children}
                        onChange={this.onChange}/>
            </GLCell>
        )
    }
}

GLCellNumber.propTypes = {
    children: PropTypes.number,
    onChange: PropTypes.func,
    className: PropTypes.string
}

GLCellNumber.defaultProps = {
    children: 0,
    onChange: () => {},
    className: ''
}

GLCellNumber.displayName = 'GLCellNumber'

export default GLCellNumber
