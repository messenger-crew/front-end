import React, { Component, PropTypes } from 'react'
import GLCell from '../default'

const TYPES = {
    DEFAULT: 'default',
    EMAIL: 'email'
}

/** @class
 * @name GLCellLink
 * @description grid cell link component
 */
class GLCellLink extends Component {
    rendeLink(value, type, target) {
        let url = ''
        if (type === TYPES.EMAIL) {
            url = `mailto:${value}`
        } else {
            url = value
        }
        const urlTarget = target || '_self'
        return (<a href={url} target={urlTarget}>{value}</a>)
    }

    render() {
        const link = this.rendeLink(
            this.props.children,
            this.props.type,
            this.props.target
        )
        const { className, ...rest } = this.props
        return (
            <GLCell className={`cell-link ${className}`} {...rest}>{link}</GLCell>
        )
    }
}

GLCellLink.propTypes = {
    value: PropTypes.bool,
    className: PropTypes.string
}

GLCellLink.defaultProps = {
    value: false,
    className: ''
}

export default GLCellLink
