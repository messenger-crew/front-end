import React, { Component, PropTypes } from 'react'
import GLCell from '../default'

const ASC = 'asc'
const DESC = 'desc'
const DEFAULT_SORT = DESC

/** @class
 * @name GLCellSort
 * @description grid cell sort component
 */
class GLCellSort extends Component {
    constructor(props) {
        super(props)
        this.onClick = this._onClick.bind(this)
    }

    getType() {
        return (this.props.type ? this.props.type.toLowerCase() : DEFAULT_SORT)
    }

    _onClick() {
        this.props.onClick(this.getType() === ASC ? DESC : ASC)
    }

    render() {
        let sortType = this.getType()
        let iconClass = 'fa fa-sort-' + sortType
        return (
            <GLCell className={`cell-sort ${this.props.className}`} onClick={this.onClick}>
                {this.props.children}
                <i className={iconClass} aria-hidden="true"></i>
            </GLCell>
        )
    }
}

GLCellSort.propTypes = {
    children: PropTypes.any,
    onClick: PropTypes.func,
    type: PropTypes.string,
    className: PropTypes.string
}

GLCellSort.defaultProps = {
    children: 0,
    onClick: () => {},
    type: DEFAULT_SORT,
    className: ''
}

GLCellSort.displayName = 'GLCellSort'

export default GLCellSort
