import React, { Component, PropTypes } from 'react'
import GLCell from '../default'

/** @class
 * @name GLCellContextMenu
 * @description grid cell context-menu component
 */
class GLCellContextMenu extends Component {
    constructor(props) {
        super(props)

        this.state = {
            showMenu: false
        }

        this.toggleMenu = this._toggleMenu.bind(this)
    }

    _toggleMenu() {
        let showMenu = !this.state.showMenu
        this.setState({ showMenu })
        if (showMenu) {
            window.addEventListener('mousedown', this.toggleMenu)
            return
        }
        window.removeEventListener('mousedown', this.toggleMenu)
    }

    render() {
        const { className, children, label, ...rest } = this.props
        let classMenu = this.state.showMenu ? 'show' : ''
        return (
            <GLCell className={`cell-context-menu ${className}`} {...rest}>
                {label}
                <i className="fa fa-ellipsis-v" onClick={this.toggleMenu}>
                    <div className={`menu-container ${classMenu}`} >
                        {children}
                    </div>
                </i>
            </GLCell>
        )
    }
}

GLCellContextMenu.displayName = 'GLCellContextMenu'

GLCellContextMenu.propTypes = {
    className: PropTypes.string,
    label: PropTypes.string,
    children: PropTypes.any
}

GLCellContextMenu.defaultProps = {
    className: '',
    label: '',
    children: null
}

export default GLCellContextMenu
