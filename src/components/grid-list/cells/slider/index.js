import React, { Component, PropTypes } from 'react'
import GBCell from '../default/'
import Slider from './../../../slider/'

/** @class
 * @name GBCellSlider
 */
class GBCellSlider extends Component {
    render() {
        const { className, children, ...rest } = this.props
        return (
            <GBCell className={`cell-slider ${className}`} {...rest}>
                <Slider onChange={this.props.onChange}>{children}</Slider>
            </GBCell>
        )
    }
}

GBCellSlider.displayName = 'GBCellSlider'

GBCellSlider.defaultProps = {
    onChange: () => {},
    children: 0,
    className: ''
}

GBCellSlider.propTypes = {
    onChange: PropTypes.func,
    children: PropTypes.number,
    className: PropTypes.string
}

export default GBCellSlider
