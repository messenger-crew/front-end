import React, { Component, PropTypes } from 'react'
import GLCell from '../default'

/** @class
 * @name GLCellCheckbox
 * @description grid cell checkbox component
 */
class GLCellCheckbox extends Component {
    render() {
        const { className, children, ...rest } = this.props
        return (
            <GLCell className={`cell-checkbox ${className}`} {...rest}>
                <input value={children} type='checkbox' />
            </GLCell>
        )
    }
}

GLCellCheckbox.propTypes = {
    children: PropTypes.bool,
    className: PropTypes.string
}

GLCellCheckbox.defaultProps = {
    children: false,
    className: ''
}

export default GLCellCheckbox
