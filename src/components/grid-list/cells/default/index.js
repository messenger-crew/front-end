import React, { Component, PropTypes } from 'react'

/** @class
 * @name GLCell
 * @description default grid cell component
 */
class GLCell extends Component {
    render() {
        const { className, children, ...rest } = this.props
        return (
            <div className={'grid-cell ' + className} {...rest}>{children}</div>
        )
    }
}

GLCell.propTypes = {
    children: PropTypes.any,
    className: PropTypes.string,
    onClick: PropTypes.func
}

GLCell.defaultProps = {
    children: 0,
    className: '',
    onClick: () => {}
}

GLCell.displayName = 'GLCell'

export default GLCell
