import React, { Component } from 'react'

/** @class
 * @name GLHeader
 */
class GLHeader extends Component {
    render() {
        return (
            <div className='grid-header'>
                {this.props.children}
            </div>
        )
    }
}

GLHeader.displayName = 'GLHeader'

export default GLHeader
