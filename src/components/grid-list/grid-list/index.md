```
const list = [{
    "_id": "58e3733d5535df42eea775a6",
    "isActive": false,
    "name": "Gould Meyers",
    "gender": "male",
    "company": "SPRINGBEE",
    "email": "gouldmeyers@springbee.com",
    "phone": "+1 (864) 514-3627",
    "address": "588 Knickerbocker Avenue, Sparkill, Marshall Islands, 216",
    "about": "Sint enim laboris qui commodo sint ullamco excepteur nisi aliqua ea est adipisicing in. Dolor consequat nostrud ut ullamco occaecat incididunt nisi ad proident dolore et proident dolor. Irure laboris Lorem officia in consequat. In et excepteur ut velit elit. Exercitation incididunt aliquip adipisicing do ullamco nulla tempor cupidatat quis. Ex nostrud cillum magna enim consectetur quis proident pariatur Lorem ipsum sunt. Qui cillum ad proident fugiat aliquip ad qui elit quis elit ea ex deserunt.\r\n",
    "tags": [
        "esse",
        "duis",
        "exercitation",
        "ad",
        "laborum",
        "excepteur",
        "ex"
    ]
}, {
    "_id": "58e3733d90df14087c97b5d2",
    "isActive": false,
    "name": "Barron Callahan",
    "gender": "male",
    "company": "ASSURITY",
    "email": "barroncallahan@assurity.com",
    "phone": "+1 (912) 584-3748",
    "address": "290 Kane Street, Tonopah, New York, 8141",
    "about": "Pariatur anim irure deserunt commodo mollit Lorem et sint ut et minim officia aliquip et. Ut nostrud consequat ex qui velit consequat. Anim Lorem aute occaecat reprehenderit ex qui cupidatat duis proident. Id exercitation laborum nisi in sunt. Nisi anim sit mollit duis laboris magna aliqua ea culpa veniam veniam. Proident ad qui consectetur est occaecat cillum duis esse reprehenderit. Magna tempor laboris velit esse dolor.\r\n",
    "tags": [
        "incididunt",
        "nulla",
        "do",
        "do",
        "elit",
        "tempor",
        "adipisicing"
    ]
}];

<GridList>
    <GLHeader>
        <GLRow>
            <GLCell>#</GLCell>
            <GLCellSort>Name</GLCellSort>
            <GLCell>Gender</GLCell>
            <GLCell>Company</GLCell>
            <GLCell>Email</GLCell>
        </GLRow>
    </GLHeader>
    <GLBody>
        {list.map((item, index) => (
            <GLRow key={index}>
                <GLCellNumber>{index + 1}</GLCellNumber>
                <GLCell>{item.name}</GLCell>
                <GLCell>{item.gender}</GLCell>
                <GLCell>{item.company}</GLCell>
                <GLCellLink type="email">{item.email}</GLCellLink>
            </GLRow>
        ))}
    </GLBody>
</GridList>
```
