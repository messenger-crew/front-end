import React, { Component } from 'react'

/** @class
 * @name GridList
 * @description main grid-list component
 */
class GridList extends Component {
    render() {
        return (
            <div className="grid-list">
                {this.props.children}
            </div>
        )
    }
}

GridList.displayName = 'GridList'

export default GridList
