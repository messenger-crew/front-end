import React, { Component } from 'react'

/** @class
 * @name Body
 * @description grid body component
 */
class GLBody extends Component {
    render() {
        return (
            <div className="grid-body">
                {this.props.children}
            </div>
        )
    }
}

GLBody.displayName = 'GLBody'

export default GLBody
