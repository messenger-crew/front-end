import React from 'react'

/** @class
 * @name ProgressBarTest
 * @description progressbar for testing
 */
class ProgressBarTest extends React.Component {
    render() {
        return (
            <div className="progress">
                <div className="progress-bar progress-bar-success" style={{width: '35%'}}>
                    <span className="sr-only">35% Complete (success)</span>
                </div>
            </div>
        )
    }
}

ProgressBarTest.displayName = 'ProgressBarTest'

export default ProgressBarTest
