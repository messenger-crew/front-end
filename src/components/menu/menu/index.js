import React, { Component, PropTypes } from 'react'

/** @class
 * @name Menu
 */
class Menu extends Component {
    render() {
        const { className, children, ...rest } = this.props
        return (
            <div className={`menu ${className}`} {...rest}>{children}</div>
        )
    }
}

Menu.displayName = 'Menu'

Menu.defaultProps = {
    children: null,
    className: ''
}

Menu.propTypes = {
    className: PropTypes.string
}

export default Menu
