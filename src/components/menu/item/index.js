import React, { Component, PropTypes } from 'react'

/** @class
 * @name MItem
 */
class MItem extends Component {
    render() {
        const { className, children, ...rest } = this.props
        return (
            <div className={`menu__item ${className}`} {...rest}>{children}</div>
        )
    }
}

MItem.displayName = 'MItem'

MItem.defaultProps = {
    children: null,
    className: ''
}

MItem.propTypes = {
    className: PropTypes.string
}

export default MItem
