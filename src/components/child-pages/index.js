import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import Test1Page from '../../containers/test-1-page'
import Test2Page from '../test-2-page'

/** @class
 * @name ChildPages
 * @description test [component]
 */
class ChildPages extends React.Component {
    render() {
        const matchUrl = (this.props.match && this.props.match.url) || ''
        return (
            <div>
                <h1>Child Pages</h1>
                <Switch>
                    <Route path={`${matchUrl}/2`} component={Test2Page} />
                    <Route path={`${matchUrl}/1`} component={Test1Page} />
                    <Redirect from={`${matchUrl}`} to={`${matchUrl}/1`} />
                </Switch>
            </div>
        )
    }
}

ChildPages.displayName = 'ChildPages'

export default ChildPages
