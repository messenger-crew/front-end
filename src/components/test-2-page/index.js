import React from 'react'

/** @class
 * @name Test2Page
 * @description test 2 page
 */
class Test2Page extends React.Component {
    render() {
        return (
            <h3>Test 2</h3>
        )
    }
}

Test2Page.displayName = 'Test2Page'

export default Test2Page
