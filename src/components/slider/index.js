import React, { Component, PropTypes } from 'react'

/** @class
 * @name Slider
 * @description A Slider control is a window containing a slider and optional tick marks.
 * You can move the slider by dragging it, clicking the mouse to either side of the slider, or using the keyboard.
 */
class Slider extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isMoving: false,
            markerPosition: 0
        }
        this.startMoveMarker = this._startMoveMarker.bind(this)
        this.stopMoveMarker = this._stopMoveMarker.bind(this)
        this.moveMarker = this._moveMarker.bind(this)
    }

    calcPosition(clientX, container) {
        let position = container.getBoundingClientRect()
        if (position.right <= clientX) {
            return 100
        }
        if (position.left >= clientX) {
            return 0
        }
        let markerPosition = clientX - position.left
        return (markerPosition / position.width) * 100
    }

    _startMoveMarker(event) {
        let markerPosition = this.calcPosition(event.clientX, this.container)
        this.setState({ isMoving: true, markerPosition })
        window.addEventListener('mousemove', this.moveMarker)
        window.addEventListener('mouseup', this.stopMoveMarker)
    }

    _stopMoveMarker() {
        this.setState({ isMoving: false })
        this.props.onChange(this.state.markerPosition)
        window.removeEventListener('mousemove', this.moveMarker)
        window.removeEventListener('mouseup', this.stopMoveMarker)
    }

    _moveMarker(event) {
        let markerPosition = this.calcPosition(event.clientX, this.container)
        this.setState({ markerPosition })
    }

    render() {
        let styleForMarker = {
            left: `${this.state.isMoving ? this.state.markerPosition : this.props.children}%`
        }
        return (
            <div className='slider' ref={(container) => this.container = container}>
                <div className='line'></div>
                <div className='marker'
                    style={styleForMarker}
                    onMouseDown={this.startMoveMarker}
                >
                    <div className='body'></div>
                </div>
            </div>
        )
    }
}

Slider.displayName = 'Slider'

Slider.defaultProps = {
    onChange: () => {},
    children: 0
}

Slider.propTypes = {
    onChange: PropTypes.func,
    children: PropTypes.number
}

export default Slider
