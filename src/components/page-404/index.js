import React from 'react'

/** @class
 * @name Page404
 * @description test 404 page
 */
class Page404 extends React.Component {
    render() {
        return (
            <div>404 page</div>
        )
    }
}

Page404.displayName = 'Page404'

export default Page404
