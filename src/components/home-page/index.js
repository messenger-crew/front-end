import React from 'react'

/** @class
 * @name HomePage
 * @description test home page
 */
class HomePage extends React.Component {
    render() {
        return (
            <div>
                <h1>Home Page</h1>
            </div>
        )
    }
}

HomePage.displayName = 'HomePage'

export default HomePage
